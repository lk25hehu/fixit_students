'use strict';

describe('REST API', () => {

  let server;
  require('chai').should();

  before((done) => {
    //Clean everything up before doing new tests
    Object.keys(require.cache).forEach((key) => delete require.cache[key]);
    let hapi = require('hapi');
    server = new hapi.Server();
    server.connection({
      host: 'localhost',
      port: 3000
    });
    require('../routes.js')(server);
    done();
  });

  const options = {
    method: 'POST',
    url: '/basket/',
    headers: {
      'Content-Type': 'application/json'
    },
  };

  const optionsDelete = {
    method: 'DELETE',
    url: '/basket/',
    headers: {
      'Content-Type': 'application/json'
    },
  };

  context('when trying to delete an item of a user basket', () => {
    it('it should reply 404 for non existing baskets', () => {

      let opt = JSON.parse(JSON.stringify(optionsDelete));
      opt.url += 'huw/fairphone1';
      return server.inject(opt).then((response) => {
        response.should.be.an('object').and.contain.keys('statusCode', 'payload', 'headers');
        response.statusCode.should.equal(404);
      });
    });

    it('it should reply with 404 for items that are not in the basket', () => {
      let opt = JSON.parse(JSON.stringify(options));
      opt.url += 'huw/fairphone2';
      return server.inject(opt).then(() => {
        let opt = JSON.parse(JSON.stringify(optionsDelete));
        opt.url += 'huw/fairphone1';
        return server.inject(opt).then((response) => {
          response.should.be.an('object').and.contain.keys('statusCode', 'payload');
          response.statusCode.should.equal(404);
        });
      });
    });

    it('it should reply with the basket after deleting an item', () => {
      let opt = JSON.parse(JSON.stringify(options));
      opt.url += 'huw/fairphone2';
      return server.inject(opt).then(() => {
        let opt = JSON.parse(JSON.stringify(optionsDelete));
        opt.url += 'huw/fairphone2';
        return server.inject(opt).then((response) => {
          response.should.be.an('object').and.contain.keys('statusCode', 'payload');
          response.statusCode.should.equal(200);
          response.payload.should.be.a('string');
          let payload = JSON.parse(response.payload);
          payload.should.be.an('object').and.be.empty;
        });
      });
    });

  });
});
